import processing.serial.*;

class PortDetector  {
  
  int detectionInterval = 2 * 1000;  // N miliseconds 2000 min for windows
  Timer timer = new Timer(detectionInterval);
  String[] ports = new String[0];
  String portNameDevice;
  String portNameShort;
  Serial[] serials = new Serial[ports.length];
  boolean deviceConnected = false;
  boolean portInitializationInProgress = false;
  boolean spectruinoDetectionInProgress = false;
  boolean detectionTimedOut = false;
  boolean mockPort = false;  // is simulation mode running?
  
  boolean portReady() {
    // is the physical port connected to spectruino?
    // meaning application just started, or application has detected spectruino
    return (deviceConnected && !spectruinoDetectionInProgress && !portInitializationInProgress); 
  }
  
  void init() {
      ports = Serial.list();
      serials = new Serial[ports.length];
      mockPort = false;
      deviceConnected = false;
  }
    
  void startPortDetection(PApplet parent) {
    String[] portFound;                  // null if spectruino serial port not found (true if port was found)
    String[] portIsSpecial;              // we do not want special serial ports, otherwise errors when opening
    init();
    detectionTimedOut = false;
    spectruinoDetectionInProgress = true;

    println("Available ports:");
    println(ports);
    
    portInitializationInProgress = true;
    for (int i=0; i<ports.length; i++) {  
      println("Probing: " + i + ports[i]);
      portFound = match(ports[i].toLowerCase(), "usb|com\\d*$"); //[uUcC][sSoO][bBmM]
      portIsSpecial = match(ports[i].toLowerCase(), "cu.");
      //portFound = match(portFound, "!!!!!!!!!!!not cu.Bluetooth  alebo cu."); //[uUcC][sSoO][bBmM]      
      if (portFound!=null && portIsSpecial==null) {
        try {
          println("Attempting: " + i + ports[i]);
          serials[i] = new Serial(parent, ports[i], bitrate);
        } catch (Exception e) {
          serials[i] = null;
          println("Problem probing port " + ports[i] + ".");
          e.printStackTrace();
          continue;
        }
        serials[i].bufferUntil(_c);        
      }// End if portFound
     }
     timer.start();
     portInitializationInProgress = false;
  }
  
  Serial checkPortDetection(Serial p) {
    println("Checking ports...");
    if (portInitializationInProgress || detectionTimedOut() || !spectruinoDetectionInProgress) {
      return null;
    } 
    else {
////    if (spectruinoDetectionInProgress) {
      byte[] portBytes = p.readBytes();
////      if (!mySight3.this.isHeaderPresent(portBytes, portBytes.length)) {
      if (!isHeaderPresent(portBytes, portBytes.length)) {  
        return null;
      }
        // close other ports
      for (int i=0; i<serials.length; i++) {
          if (serials[i]==p) 
          {
            // found spectruino on port
            //if (_DBG) {
              println("Spectruino on port Nr. ["+i+"] "+ ports[i] );
              //printMainText("\nSpectruino connected on port "+ports[i]);
              portNameDevice = new String(ports[i]);
              String tmp3[] = match(ports[i], "(?<=-).*$|com"); // |com
              if (tmp3!=null) {
                portNameShort = tmp3[0];
              } else {
                portNameShort = "unknown";
              }
              
            //}
            spectruinoDetectionInProgress = false;
          } else {
            if (serials[i]!=null) {
              println("Closing port: " + i);
              serials[i].stop();
             }
          } 
      }
      deviceConnected = true;      
      return p;
    } 
    /*else {
      deviceConnected = false;
      return null;
    } */
  }
  
  boolean detectionTimedOut() {
    if (!detectionTimedOut && spectruinoDetectionInProgress && timer.isFinished()) {
      for (int i=0; i<serials.length; i++) {
        if (serials[i]!=null) {
          println("Detection timeout on [" +i);
          serials[i].stop();
        }
      }
      detectionTimedOut = true;
      println("timer timed out");
    }
    return detectionTimedOut;
  }
}
